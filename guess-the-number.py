import random

num = ''.join(map(str, random.sample(range(10), 4)))
print("Start")
print("---------------------------------")

finish = 0

while finish < 10:

    tmp = input('> ')
    while len(tmp) > 4:
        print("You entered more than 4 numbers")
        tmp = input('> ')
            
    
    if tmp.lower() == 'quit':
        break

    if tmp == num:
        print('you win')
        break

    place = [i for i, j in zip(num, tmp) if i == j]
    thereIs = 0
    for i in tmp:
        if(i in num):
            thereIs += 1


    print("There is {}".format(thereIs))
    print("In its place {}".format(len(place)))
    print("==================================")

    finish += 1